Recreate the Fake API demo in Lesson 3.10.

Change the data structure from a List<Guitar> to a Map<Integer, Guitar> where Integer references the id of a particular guitar.

Use the built in DI container to inject the GuitarRespository dependency into the controller.

Modify the existing project to return ResponseEntity<T> with appropriate HTTP status codes.
You should also cater for common exceptions such as the resource not being found (404).

Create a DTO which return a summary of the amount of guitars stored for each guitar brand on the server.
There should be a GET endpoint with the URI ("/v1/api/guitars/brands") which serves this DTO to the client.