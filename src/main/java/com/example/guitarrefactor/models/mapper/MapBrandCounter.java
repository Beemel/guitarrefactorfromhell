package com.example.guitarrefactor.models.mapper;

import com.example.guitarrefactor.models.domain.Guitar;
import com.example.guitarrefactor.models.dto.BrandCounterDto;


import java.util.HashMap;
import java.util.Map;

public class MapBrandCounter {

private static int brandCounter (Map<Integer, Guitar> guitars, String brand) {
    return (int) guitars
            .values()
            .stream()
            .filter(g -> g.getBrand().equals(brand))
            .count();
 }
public static BrandCounterDto mapBrandCounter (Map<Integer, Guitar>guitars) {
    Map<String, Integer> mapBrands = new HashMap<>();
    for (Guitar guitar: guitars.values()){
        String brand = guitar.getBrand();
        mapBrands.put (brand, brandCounter(guitars, brand));
    }
    return new BrandCounterDto(mapBrands);

}
}


