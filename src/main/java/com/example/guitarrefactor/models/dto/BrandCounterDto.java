package com.example.guitarrefactor.models.dto;
import java.util.*;

public class BrandCounterDto {

    private Map<String, Integer> brandCounter;


    public BrandCounterDto(Map<String, Integer> brandCounter) {
        this.brandCounter = brandCounter;
    }


    public Map<String, Integer> getBrandCounter() {
        return brandCounter;
    }

    public void setBrandCounter(Map<String, Integer> brandCounter) {
        this.brandCounter = brandCounter;
    }




}









