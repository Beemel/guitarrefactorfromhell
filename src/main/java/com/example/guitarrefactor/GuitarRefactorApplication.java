package com.example.guitarrefactor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GuitarRefactorApplication {

    public static void main(String[] args) {
        SpringApplication.run(GuitarRefactorApplication.class, args);
    }

}
