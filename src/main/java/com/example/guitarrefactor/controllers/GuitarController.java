package com.example.guitarrefactor.controllers;
import com.example.guitarrefactor.dataaccess.IGuitarRepository;
import com.example.guitarrefactor.models.domain.Guitar;
import com.example.guitarrefactor.models.dto.BrandCounterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Map;


@RestController
@RequestMapping(value="/guitars")
public class GuitarController {
    @Autowired
    private IGuitarRepository guitarRepository;

    @GetMapping
    public ResponseEntity<Map<Integer, Guitar>> getAllGuitars() {
        return new ResponseEntity<>(guitarRepository.getAllGuitars(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Guitar> getGuitar(@PathVariable Integer id) {
        if (!guitarRepository.guitarExist(id)==true) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }else{
            return new ResponseEntity<>(guitarRepository.getGuitar(id), HttpStatus.OK);
        }
    }

    @PostMapping("/{id}")
    public ResponseEntity <Guitar> addGuitar(@PathVariable Integer id, @RequestBody Guitar guitar) {

        return new ResponseEntity <>(guitarRepository.addGuitar(id, guitar), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity <Guitar> replaceGuitar(@PathVariable Integer id, @RequestBody Guitar guitar) {
        return new ResponseEntity <>(guitarRepository.replaceGuitar(id, guitar), HttpStatus.CREATED);
    }

    @PatchMapping("/{id}")
    public ResponseEntity <Guitar> modifyGuitar(@PathVariable Integer id, @RequestBody Guitar guitar) {
        return new ResponseEntity <>(guitarRepository.modifyGuitar(id, guitar), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{id}")
    public String deleteGuitar(@PathVariable Integer id) {
        guitarRepository.deleteGuitar(id);
        return "Guitar " + id + " deleted";
    }

    @GetMapping("/brand")
    public ResponseEntity <BrandCounterDto> brandList() {
        return new ResponseEntity<>(guitarRepository.getBrandCounter(), HttpStatus.OK);
    }
}