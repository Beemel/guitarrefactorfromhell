package com.example.guitarrefactor.dataaccess;
import com.example.guitarrefactor.models.domain.Guitar;
import com.example.guitarrefactor.models.dto.BrandCounterDto;
import com.example.guitarrefactor.models.mapper.MapBrandCounter;
import org.springframework.stereotype.Component;
import java.util.HashMap;
import java.util.Map;

@Component
public class GuitarRepository implements IGuitarRepository {

    private Map<Integer, Guitar> guitarMap = originalListOfGuitars();

    private Map<Integer, Guitar> originalListOfGuitars() {
        var guitars = new HashMap<Integer, Guitar>();
        guitars.put(1, new Guitar("Fender", "Telecaster"));
        guitars.put(2, new Guitar("Dean Guitars", "Tyrant Bloodstorm"));
        guitars.put(3, new Guitar("Gibson", "Explorer"));
        guitars.put(4, new Guitar("Martin", "LX1E Little Martin"));

        return guitars;
    }

    @Override
    public Map<Integer, Guitar> getAllGuitars() {
        return guitarMap;

    }
    @Override
    public Guitar getGuitar(Integer id) {
        return guitarMap.get(id);
    }

    @Override
    public Guitar addGuitar(Integer id, Guitar guitar) {
        if (guitarMap.containsKey(id)){
            return null;
        }else{
            guitarMap.put(id, new Guitar(guitar.getBrand(), guitar.getModel()));
        return guitarMap.get(id);}
    }

    @Override
    public Guitar replaceGuitar(Integer id, Guitar guitar) {
        guitarMap.remove(id);
        guitarMap.put(id, new Guitar(guitar.getBrand(), guitar.getModel()));
        return guitarMap.get(id);
    }

    @Override
    public Guitar modifyGuitar(Integer id, Guitar guitar) {
        var guitarToModify = getGuitar(id);
        guitarToModify.setBrand(guitar.getBrand());
        guitarToModify.setModel(guitar.getModel());
        return guitarToModify;
    }

    @Override
    public void deleteGuitar(Integer id) {
        guitarMap.remove(id);
    }
    @Override
    public Boolean guitarExist (Integer id) {
        if (getGuitar(id) != null) {
            return true;
        }else{
            return false;
        }
    }

    @Override
    public BrandCounterDto getBrandCounter() {
        return MapBrandCounter.mapBrandCounter(guitarMap);
    }
}





