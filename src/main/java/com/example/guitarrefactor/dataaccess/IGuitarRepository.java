package com.example.guitarrefactor.dataaccess;

import com.example.guitarrefactor.models.domain.Guitar;
import com.example.guitarrefactor.models.dto.BrandCounterDto;

import java.util.Map;

public interface IGuitarRepository {
    Map<Integer, Guitar> getAllGuitars();
    Guitar getGuitar (Integer id);
    Guitar addGuitar (Integer id, Guitar guitar);
    Guitar replaceGuitar (Integer id, Guitar guitar);
    Guitar modifyGuitar (Integer id, Guitar guitar);
    void deleteGuitar (Integer id);
    Boolean guitarExist (Integer id);
    BrandCounterDto getBrandCounter ();




}
